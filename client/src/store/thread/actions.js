import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  DELETE_POST: 'thread/delete-post',
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  UPDATE_COMMENT: 'thread/update-comment'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const updatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const removePost = createAction(ActionType.DELETE_POST, id => ({
  payload: {
    id
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const deletePost = id => async dispatch => {
  const res = await postService.deletePost(id);
  if (res.count === 1) {
    dispatch(removePost(id));
  }
};

const putPost = post => async dispatch => {
  const res = await postService.updatePost(post);
  if (res) {
    dispatch(updatePost(post));
  }
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const res = await postService.likePost(postId);
  const { id, createdAt, updatedAt } = res;
  const { postReactions } = await postService.getPost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislike = createdAt !== updatedAt ? 1 : 0;
  const mapLikes = post => ({
    ...post,
    likeCount: String(Number(post.likeCount) + diff),
    dislikeCount: String(Number(post.dislikeCount) - diffDislike),
    postReactions: [...postReactions] // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const { postReactions } = await postService.getPost(postId);
  const diff = id ? 1 : -1;
  const diffLike = createdAt !== updatedAt ? 1 : 0;
  const mapLikes = post => ({
    ...post,
    dislikeCount: String(Number(post.dislikeCount) + diff),
    likeCount: String(Number(post.likeCount) - diffLike),
    postReactions: [...postReactions]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: String(Number(post.commentCount) + 1),
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const { count } = await commentService.deleteComment(commentId);

  if (count === 1) {
    const mapComments = post => ({
      ...post,
      commentCount: String(Number(post.commentCount) - 1),
      comments: [...(post.comments || [])].filter(c => c.id !== commentId)
    });

    const {
      posts: { posts, expandedPost }
    } = getRootState();

    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    dispatch(setPosts(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
      dispatch(setExpandedPost(mapComments(expandedPost)));
    }
  }
};

const updateComment = body => async (dispatch, getRootState) => {
  const res = await commentService.updateComment(body);
  const comment = await commentService.getComment(res.id);

  if (comment) {
    const mapComments = post => ({
      ...post,
      comments: [...(post.comments || [])].map(c => (c.id === comment.id ? comment : c))
    });

    const {
      posts: { posts, expandedPost }
    } = getRootState();

    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));
    dispatch(setPosts(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
      dispatch(setExpandedPost(mapComments(expandedPost)));
    }
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  removePost,
  updatePost,
  putPost,
  deletePost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  deleteComment,
  updateComment
};
