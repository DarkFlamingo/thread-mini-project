const ButtonSize = {
  LARGE: 'large',
  SMALL: 'small',
  MINI: 'mini'
};

export { ButtonSize };
