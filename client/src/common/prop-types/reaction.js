import PropTypes from 'prop-types';
import { imageType } from 'src/common/prop-types/image';

const reactionType = PropTypes.exact({
  id: PropTypes.string.isRequired,
  isLike: PropTypes.bool.isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  user: PropTypes.exact({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    image: imageType
  }).isRequired
});

export { reactionType };
