import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import {
  ExpandedPost,
  SharedPostLink,
  AddPost,
  UpdatePost
} from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  userIdReaction: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [updatePost, setUpdatePost] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showPostsLikedByMe, setShowPostsLikedByMe] = React.useState(false);

  const dispatch = useDispatch();

  const handleDeletePost = React.useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  const hadleUpdatePost = React.useCallback(
    post => dispatch(threadActionCreator.putPost(post)),
    [dispatch]
  );

  const handlePostLike = React.useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = React.useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = React.useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowPostsLikedByMe = () => {
    setShowPostsLikedByMe(!showPostsLikedByMe);
    postsFilter.userIdReaction = showPostsLikedByMe ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const toggleUpdatePost = post => {
    setUpdatePost(post);
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Show posts liked by me"
          checked={showPostsLikedByMe}
          onChange={toggleShowPostsLikedByMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            isActiveBtn={userId === post.user.id}
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onPostDelete={handleDeletePost}
            onPostUpdate={toggleUpdatePost}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {updatePost && (
        <UpdatePost
          post={updatePost}
          close={() => setUpdatePost(undefined)}
          onUpdatePost={hadleUpdatePost}
        />
      )}
    </div>
  );
};

export default Thread;
