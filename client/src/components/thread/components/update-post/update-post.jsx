import * as React from 'react';
import PropTypes from 'prop-types';
import { postType } from 'src/common/prop-types/prop-types';
import { Modal, Button, Form } from 'src/components/common/common';
import {
  IconName,
  ButtonColor,
  ButtonSize,
  ButtonFormCircle,
  ButtonType
} from 'src/common/enums/enums';

import styles from './styles.module.scss';

const UpdatePost = ({ post, close, onUpdatePost }) => {
  const { body } = post;

  const [newBody, setNewBody] = React.useState(body);

  const handlePostUpdate = () => {
    close();
    return onUpdatePost({ ...post, body: newBody });
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Update Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handlePostUpdate}>
          <Form.TextArea
            name="body"
            value={newBody}
            placeholder="Update post"
            onChange={ev => setNewBody(ev.target.value)}
          />
          <Button
            circular={ButtonFormCircle.CIRCLE}
            color={ButtonColor.TEAL}
            iconName={IconName.SYNC}
            size={ButtonSize.MINI}
            type={ButtonType.SUBMIT}
          >
            {' '}
            Update
          </Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: postType.isRequired,
  close: PropTypes.func.isRequired,
  onUpdatePost: PropTypes.func.isRequired
};

export default UpdatePost;
