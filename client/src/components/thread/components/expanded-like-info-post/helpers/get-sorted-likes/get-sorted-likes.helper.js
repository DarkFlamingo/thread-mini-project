import { getDiff } from 'src/helpers/helpers';

const getSortedLikes = likes => likes.slice().sort((a, b) => getDiff(a.updatedAt, b.updatedAt));

export { getSortedLikes };
