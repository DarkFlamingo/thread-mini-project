import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import {
  // Spinner,
  // Post,
  Modal
  // Comment as CommentUI
} from 'src/components/common/common';
// import AddComment from '../add-comment/add-comment';
// import Comment from '../comment/comment';
import { getSortedLikes } from './helpers/helpers';

const ExpandedLikeInfoPost = () => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedLikeInfoPost
  }));

  const handleExpandedLikeInfoPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedLikeInfoPost(id)),
    [dispatch]
  );

  const handleExpandedLikeInfoPostClose = () => handleExpandedLikeInfoPostToggle();

  const sortedLikes = getSortedLikes(post.postReactions ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedLikeInfoPostClose}
    >
      {/* {post ? (
        <Modal.Content>
          <Post
            isActiveBtn={false}
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment key={comment.id} comment={comment} />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )} */}
      {post ? <Modal.Content>{sortedLikes.map(el => <div>{el.id}</div>)}</Modal.Content> : <div>No post</div>}
    </Modal>
  );
};

export default ExpandedLikeInfoPost;
