import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import {
  Comment as CommentUI,
  Button,
  Form
} from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import {
  IconName,
  ButtonColor,
  ButtonSize,
  ButtonFormCircle
} from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Comment = ({
  comment,
  onCommentDelete,
  onCommentUpdate,
  isActiveBtn
}) => {
  const [updatedCommentText, setUpdatedCommentText] = React.useState(
    comment.body
  );
  const [editMode, setEditMode] = React.useState(false);

  const hadleCommentDelete = () => onCommentDelete(comment.id);
  const handleUpdateComment = () => onCommentUpdate({ ...comment, body: updatedCommentText });

  const activeEditMode = () => {
    setEditMode(true);
  };

  const deactiveEditMode = () => {
    setEditMode(false);
    handleUpdateComment();
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={comment.user.image?.link ?? DEFAULT_USER_AVATAR} />
      <div className={styles.commentWrapper}>
        <CommentUI.Content className={styles.w100}>
          <CommentUI.Author as="a">{comment.user.username}</CommentUI.Author>
          <CommentUI.Metadata>
            {getFromNowTime(comment.createdAt)}
          </CommentUI.Metadata>
          {editMode && isActiveBtn ? (
            <Form onSubmit={handleUpdateComment}>
              <Form.TextArea
                value={updatedCommentText}
                placeholder="Type a comment..."
                onChange={ev => setUpdatedCommentText(ev.target.value)}
                onBlur={deactiveEditMode}
              />
            </Form>
          ) : (
            <CommentUI.Text onDoubleClick={activeEditMode}>
              {comment.body}
            </CommentUI.Text>
          )}
        </CommentUI.Content>
        <CommentUI.Content>
          {isActiveBtn && (
            <Button
              circular={ButtonFormCircle.CIRCLE}
              color={ButtonColor.RED}
              iconName={IconName.DELETE}
              size={ButtonSize.MINI}
              onClick={hadleCommentDelete}
            >
              {' '}
            </Button>
          )}
        </CommentUI.Content>
      </div>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentDelete: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  isActiveBtn: PropTypes.bool.isRequired
};

export default Comment;
