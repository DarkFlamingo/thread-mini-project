import * as React from 'react';
import { Image } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

import styles from './styles.module.scss';

const Like = reaction => {
  const {
    reaction: { user }
  } = reaction;

  return (
    <div className={styles.likeWrapper}>
      <Image
        className={styles.likeWrapper}
        circular
        width="20"
        height="20"
        src={user.image?.link ?? DEFAULT_USER_AVATAR}
      />
      {user.username && user.username.length > 6
        ? user.username.slice(0, 5).concat('...')
        : user.username}
    </div>
  );
};

export default Like;
