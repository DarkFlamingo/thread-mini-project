import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import {
  IconName,
  ButtonColor,
  ButtonSize,
  ButtonFormCircle
} from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import {
  Icon,
  Card,
  Image,
  Label,
  Button,
  Popup,
  Like
} from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({
  isActiveBtn,
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  onPostDelete,
  onPostUpdate,
  sharePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostDelete = () => onPostDelete(id);
  const handlePostUpdate = () => onPostUpdate(post);

  const likes = postReactions.filter(r => r.isLike === true).map(r => <Like key={r.id} reaction={r} />);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <div className={styles.cardWrapper}>
          <Card.Meta>
            <span className="date">
              posted by
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          {isActiveBtn && (
            <div className={styles.buttonGroup}>
              <Button
                circular={ButtonFormCircle.CIRCLE}
                color={ButtonColor.TEAL}
                iconName={IconName.SYNC}
                size={ButtonSize.MINI}
                onClick={handlePostUpdate}
              >
                {' '}
                Update
              </Button>
              <Button
                circular={ButtonFormCircle.CIRCLE}
                color={ButtonColor.RED}
                iconName={IconName.DELETE}
                size={ButtonSize.MINI}
                onClick={handlePostDelete}
              >
                {' '}
              </Button>
            </div>
          )}
        </div>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostLike}
            >
              <Icon name={IconName.THUMBS_UP} />
              {likeCount}
            </Label>
          )}
          content={<div className={styles.likeWrapper}>{likes}</div>}
          position="bottom center"
        />
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  isActiveBtn: PropTypes.bool.isRequired,
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func,
  onPostUpdate: PropTypes.func
};

Post.defaultProps = {
  onPostDelete: () => {},
  onPostUpdate: () => {}
};

export default Post;
