class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  delete(id) {
    return this._commentRepository.deleteById(id);
  }

  update(id, data) {
    return this._commentRepository.updateById(id, data);
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }
}

export { Comment };
