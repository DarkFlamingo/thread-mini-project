import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    userModel,
    imageModel,
    postReactionModel
  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
  }

  async getPosts(filter) {
    const { from: offset, count: limit, userId, userIdReaction } = filter;

    let where = {};
    if (userId) {
      Object.assign(where, { userId });
    }

    if (userIdReaction) {
      const posts = await sequelize.query(`(SELECT "posts"."id" FROM "posts" 
        INNER JOIN "postReactions" 
        ON "posts"."id"="postReactions"."postId" 
        WHERE "postReactions"."userId"='${userIdReaction}' AND "isLike"='true' AND "deletedAt" IS NULL)`
      );

      const updatedPosts = posts[0].map(el => el.id);

      Object.assign(where, {
        ...where,
        id: updatedPosts
      });
    }

    const res = await this.model.findAll({
      where,
      attributes: {
        include: [
          [
            sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`),
            'commentCount'
          ],
          [
            sequelize.literal(
              `(SELECT COUNT(*) FROM "postReactions" as "reaction" WHERE "reaction"."isLike"='true' AND "post"."id"="reaction"."postId")`
            ),
            'likeCount'
          ],
          [
            sequelize.literal(
              `(SELECT COUNT(*) FROM "postReactions" as "reaction" WHERE "reaction"."isLike"='false' AND "post"."id"="reaction"."postId")`
            ),
            'dislikeCount'
          ]
        ]
      },
      include: [
        {
          model: this._imageModel,
          attributes: ['id', 'link']
        },
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._postReactionModel,
          attributes: ['id', 'isLike', 'createdAt', 'updatedAt'],
          include: {
            model: this._userModel,
            attributes: ['id', 'username'],
            include: {
              model: this._imageModel,
              attributes: ['id', 'link']
            }
          },
          duplicating: false
        }
      ],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'postReactions.id',
        'user->image.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });

    return res;
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [
            sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`),
            'commentCount'
          ],
          [
            sequelize.literal(
              `(SELECT COUNT(*) FROM "postReactions" as "reaction" WHERE "reaction"."isLike"='true' AND "post"."id"="reaction"."postId")`
            ),
            'likeCount'
          ],
          [
            sequelize.literal(
              `(SELECT COUNT(*) FROM "postReactions" as "reaction" WHERE "reaction"."isLike"='false' AND "post"."id"="reaction"."postId")`
            ),
            'dislikeCount'
          ]
        ]
      },
      include: [
        {
          model: this._commentModel,
          include: {
            model: this._userModel,
            attributes: ['id', 'username'],
            include: {
              model: this._imageModel,
              attributes: ['id', 'link']
            }
          }
        },
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._imageModel,
          attributes: ['id', 'link']
        },
        {
          model: this._postReactionModel,
          attributes: ['id', 'isLike', 'createdAt', 'updatedAt'],
          include: {
            model: this._userModel,
            attributes: ['id', 'username']
          }
        }
      ]
    });
  }
}

export { Post };
